// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: [
    "~/assets/style/main.css",
    "mapbox-gl/dist/mapbox-gl.css",
    "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css",
  ],
  runtimeConfig: {
    public: {
      mapboxToken: process.env.MAPBOX_TOKEN,
    },
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
});
