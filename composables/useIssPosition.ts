type Position = {
  latitude: number;
  longitude: number;
  visibility: "daylight" | "eclipsed";
};

export const useIssPosition = async () => {
  const { data: issPositionData, refresh: issPositionRefresh } =
    await useFetch<Position>("http://127.0.0.1:3001/iss/position");

  return {
    issPositionData,
    issPositionRefresh,
  };
};
