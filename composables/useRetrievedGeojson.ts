export const useRetrievedGeojsonState = () => {
  return useState<string>("retrievedGeojson");
};

export const useRetrievedGeojson = async (name: string) => {
  const { data, status } = await useFetch<string>(
    `http://127.0.0.1:3001/geojson`,
    {
      query: { name },
    }
  );

  if (status.value === "success" && data.value) {
    useRetrievedGeojsonState().value = data.value;
  }
};
