type SunTimeframe = {
  start_time: number;
  end_time: number;
};

export const useIssSunTimeFrame = async () => {
  const {
    data: issPositionSunTimeframes,
    refresh: issPositionSunTimeframesRefresh,
  } = await useFetch<SunTimeframe[]>("http://127.0.0.1:3001/iss/sun", {
    params: {
      limit: 3,
    },
  });

  return {
    issPositionSunTimeframes,
    issPositionSunTimeframesRefresh,
  };
};
