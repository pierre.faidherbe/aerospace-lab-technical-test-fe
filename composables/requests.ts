export const postGeojson = async (name: string, geojson: string) => {
  const data = await useFetch(`http://localhost:3001/geojson/${name}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: geojson,
  });
  return data;
};
